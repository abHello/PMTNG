package automation;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.Test;

import utility.Data_Provider;




public class NewTest {
	
	
	
  @Test(dataProviderClass=Data_Provider.class,dataProvider="urlProvider")
  public void TC_1(String url) {
	  //String url="https://www.rajapack.de";
	  
		System.setProperty("webdriver.chrome.driver", "/Users/Shared/ChromeSelenium/chromedriver");
		WebDriver driver=new ChromeDriver();
		//WebDriver driver=new SafariDriver();
		
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get(url);
		//driver.findElement(By.xpath("//*[@id=\"home\"]/div[1]/header/section[2]/div/div/div/div[6]/div[2]")).click();
		driver.findElement(By.xpath("//div[@class='quickaccess quickaccess__account-noconnected']")).click();

		//Assert.assertNotEquals("", driver.findElement(By.xpath("//*[@id=\"section_main\"]/div[2]/div[1]/div/h1")).getText());
		Assert.assertNotEquals("", driver.findElement(By.xpath("//h1[@class='block__title blue no-pd']")).getText());
		System.out.println(driver.findElement(By.xpath("//h1[@class='block__title blue no-pd']")).getText());

															  

		driver.quit();
	  
	  //String url=System.getProperty("url");
		//System.out.println("url is : "+url);
		
  }
}
