package utility;

import org.testng.annotations.DataProvider;

import automation.NewTest;

public class Data_Provider {
	
	 public static final String urlProvider = "urlProvider" ;
	 
	     @DataProvider(name=urlProvider)

	     public static Object [][] urlProvider() {
	 
	         return new Object[][] {
	 
	                {"https://www.raja.fr"},
	                {"https://www.rajapack.it"},
	                {"https://www.rajapack.co.uk"},
	                {"https://www.rajapack.be/nl_BE/"},
	                {"https://www.rajapack.nl"},
	                {"https://www.rajapack.de"},
	                {"https://www.rajapack.at"},
	                {"https://www.rajapack.es"},
	                {"https://www.rajapack.pt"},
	                {"https://www.rajapack.sk"},
	                {"https://www.rajapack.se"},
	                {"https://www.rajapack.pl"},
	                {"https://www.rajapack.no"},
	                {"https://www.rajapack.dk"},
	                {"https://www.rajapack.ch/fr_CH/"},
	                {"https://www.rajapack.cz"}	 
	 
	         } ;
	 
	     }

}
